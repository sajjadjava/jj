import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util .ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.Collections;
public class serializer {
    public static void serializ(HashMap data)
    {
        int i=2;
        int shomar=0;
        System.out.println("{");
        Set<String> keys=data.keySet();
        List<String> keys1 = new LinkedList<>(keys);
        for (String key : keys1)
        {
            shomar++;
            System.out.printf("  \"%s\": ",key);
            if (data.get(key) instanceof HashMap)
            {
                System.out.println();
                int z=i+2;
                HashMap dadeh=new HashMap((HashMap)data.get(key));
                map_serializ(dadeh,z);
                if (shomar != keys1.size())
                    System.out.println(",");
                else
                    System.out.println();
            }
            else if (data.get(key) instanceof ArrayList)
            {
                System.out.println();
                int z=i+2;
                ArrayList<Object> dadeh = new ArrayList<Object>((ArrayList)data.get(key));
                array_serializ(dadeh,z);
                if (shomar != keys1.size())
                    System.out.println(",");
                else
                    System.out.println();
            }
            else if (data.get(key) instanceof String)
            {
                System.out.printf("\"%s\"",data.get(key));
                if (shomar != keys1.size())
                {
                    System.out.println(",");
                }
                else
                {
                    System.out.println();
                }
            }
            else
            {
                if (data.get(key) instanceof Double)
                    System.out.print(tabdil((double)data.get(key)));
                else
                    System.out.print(data.get(key));
                if (shomar != keys1.size())
                {
                    System.out.println(",");
                }
                else
                {
                    System.out.println();
                }
            }
        }
        System.out.print("}");
    }
    public static void map_serializ(HashMap dadeh,int i)
    {
        for (int j=0;j<i-2;j++)
            System.out.print(" ");
        int shomar=0;
        System.out.println("{");
        Set<String> keys=dadeh.keySet();
        List<String> newkeys = new LinkedList<>(keys);
        for (String key : newkeys)
        {
            shomar++;
            for (int jj=0;jj<i;jj++)
                System.out.print(" ");
            System.out.printf("\"%s\": ",key);
            if (dadeh.get(key) instanceof HashMap)
            {
                System.out.println();
                int z=i+2;
                HashMap dadeh1=new HashMap((HashMap)dadeh.get(key));
                map_serializ(dadeh1,z);
                if (shomar != newkeys.size())
                    System.out.println(",");
                else
                    System.out.println();
            }
            else if (dadeh.get(key) instanceof ArrayList)
            {
                System.out.println();
                int z=i+2;
                ArrayList<Object> dadeh1 = new ArrayList<Object>((ArrayList)dadeh.get(key));
                array_serializ(dadeh1,z);
                if (shomar != newkeys.size())
                    System.out.println(",");
                else
                    System.out.println();
            }
            else if (dadeh.get(key) instanceof String)
            {
                System.out.printf("\"%s\"",dadeh.get(key));
                if (shomar != newkeys.size())
                {
                    System.out.println(",");
                }
                else
                {
                    System.out.println();
                }
            }
            else
            {
                if (dadeh.get(key) instanceof Double)
                    System.out.print(tabdil((double)dadeh.get(key)));
                else
                    System.out.print(dadeh.get(key));
                if (shomar != newkeys.size())
                {
                    System.out.println(",");
                }
                else
                {
                    System.out.println();
                }
            }
        }
        for (int j=0;j<i-2;j++)
            System.out.print(" ");
        System.out.print("}");
    }
    public static void array_serializ(ArrayList<Object> dadeh, int i)
    {
        for (int j=0;j<i-2;j++)
            System.out.print(" ");
        System.out.print("[");
        System.out.println();
        for (int ii=0;ii<dadeh.size();ii++)
        {
            if (dadeh.get(ii) instanceof HashMap)
            {
                int z=i+2;
                HashMap dadeh1=new HashMap((HashMap)dadeh.get(ii));
                map_serializ(dadeh1,z);
                if (ii != dadeh.size()-1)
                    System.out.println(",");
                else
                    System.out.println();
            }
            else if (dadeh.get(ii) instanceof ArrayList)
            {
                int z=i+2;
                ArrayList<Object> dadeh1 = new ArrayList<Object>();
                array_serializ(dadeh1,z);
                if (ii != dadeh.size()-1)
                    System.out.println(",");
                else
                    System.out.println();
            }
            else if (dadeh.get(ii) instanceof String)
            {
                for (int jj=0;jj<i;jj++)
                {
                    System.out.print(" ");
                }
                System.out.printf("\"%s\"",(String)dadeh.get(ii));
                if (ii <= dadeh.size()-2)
                    System.out.println(",");
                else
                    System.out.println();
            }
            else
            {
                for (int jj=0;jj<i;jj++)
                {
                    System.out.print(" ");
                }
                if (dadeh.get(ii) instanceof Double)
                    System.out.print(tabdil((double)dadeh.get(ii)));
                else
                    System.out.print(dadeh.get(ii));
                if (ii <= dadeh.size()-2)
                    System.out.println(",");
                else
                    System.out.println();
            }
        }
        for (int j=0;j<i-2;j++)
            System.out.print(" ");
        System.out.print("]");
    }
    public static Object tabdil(double vorodi)
    {
        if (vorodi-((double)((int)vorodi)) == 0.0)
            return ((Integer)((int)vorodi));
        else
            return (Double)vorodi;
    }
}
