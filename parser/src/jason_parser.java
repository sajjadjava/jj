import com.sun.istack.internal.NotNull;
import java.lang.Math;
import java.nio.file.Path;
import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.io.IOException;
import java.util.ArrayList;
public class jason_parser {
    private static StringBuilder file1=new StringBuilder();
    private static StringBuilder file=new StringBuilder();
    private static HashMap data=new HashMap();
    public static HashMap tarjome(Path path)
    {
        try (Scanner in=new Scanner(path)){
            while(in.hasNext())
            {
                file1.append(in.nextLine());
            }
            file = file.append(tabdil(file1));
            for (int i=1;i<file.length();i++)
            {
                if (file.charAt(i) == '"')
                {
                    int j=i;
                    i++;
                    while (file.charAt(i) != '"')
                    {
                        i++;
                    }
                    String key=file.substring(j+1,i);
                    if (file.charAt(i+2) == '"')
                    {
                        i=i+2;
                        j=i;
                        i++;
                        while (file.charAt(i) != '"')
                        {
                            i++;
                        }
                        String value=file.substring(j+1,i);
                        data.put(key,value);
                    }
                    else if (file.charAt(i+2) == 't' || file.charAt(i+2)=='f')
                    {
                        if (file.charAt(i+2) == 't')
                        {
                            boolean arzesh = true;
                            data.put(key,arzesh);
                        }
                        if (file.charAt(i+2) == 'f')
                        {
                            boolean arzesh = false;
                            data.put(key,arzesh);
                        }
                    }
                    else if ((47<file.charAt(i+2) && file.charAt(i+2)<58) || file.charAt(i) == '-')
                    {
                        i=i+2;
                        double meghdar=0.0;
                        int tavan=0;
                        int shart=0;
                        int shart2=0;
                        if (file.charAt(i) == '-')
                        {
                            i++;
                            shart2=1;
                        }
                        while((47<file.charAt(i) && file.charAt(i)<58) || file.charAt(i) == '.') {
                            if (file.charAt(i) != '.' && shart == 0)
                                meghdar = meghdar * 10 + (file.charAt(i) - 48);
                            else {
                                i++;
                                shart = 1;
                                while (47 < file.charAt(i) && file.charAt(i) < 58) {
                                    meghdar = meghdar * 10 + (file.charAt(i) - 48);
                                    tavan++;
                                    i++;
                                }
                            }
                            if (shart == 0)
                                i++;
                        }
                        meghdar=meghdar/(Math.pow(10,tavan));
                        if (shart2 == 1)
                            meghdar=-1 * meghdar;
                        i--;
                        data.put(key,meghdar);
                    }
                    else if (file.charAt(i+2) == '{')
                    {
                        i=i+2;
                        data.put(key,analizor(i));
                        int new_i=1;
                        while (new_i != 0)
                        {
                            i++;
                            if (file.charAt(i) == '{')
                                new_i++;
                            if (file.charAt(i) == '}')
                                new_i--;
                        }
                    }
                    else
                    {
                        i=i+2;
                        data.put(key,analiz(i));
                        int new_i=1;
                        while (new_i != 0)
                        {
                            i++;
                            if (file.charAt(i) == '[')
                                new_i++;
                            if (file.charAt(i) == ']')
                                new_i--;
                        }
                    }
                }
            }
            return data;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return data;
        }
    }
    public static HashMap analizor(int i)
    {
        HashMap dadeh = new HashMap();
        i++;
        while (file.charAt(i) != '}')
        {
            if (file.charAt(i) == '"') {
                int j = i;
                i++;
                while (file.charAt(i) != '"') {
                    i++;
                }
                String key = file.substring(j + 1, i);
                if (file.charAt(i + 2) == '"') {
                    i = i + 2;
                    j = i;
                    i++;
                    while (file.charAt(i) != '"') {
                        i++;
                    }
                    String value = file.substring(j + 1, i);
                    dadeh.put(key, value);
                } else if (file.charAt(i + 2) == 't' || file.charAt(i + 2) == 'f') {
                    if (file.charAt(i + 2) == 't') {
                        boolean arzesh = true;
                        dadeh.put(key, arzesh);
                    }
                    if (file.charAt(i + 2) == 'f') {
                        boolean arzesh = false;
                        dadeh.put(key, arzesh);
                    }
                    i=i+1;
                } else if ((47 < file.charAt(i + 2) && file.charAt(i + 2) < 58) || file.charAt(i) == '-') {
                    i=i+2;
                    double meghdar=0;
                    int tavan=0;
                    int shart=0;
                    int shart2=0;
                    if (file.charAt(i) == '-')
                    {
                        i++;
                        shart2=1;
                    }
                    while((47<file.charAt(i) && file.charAt(i)<58) || file.charAt(i) == '.')
                    {
                        if (file.charAt(i) != '.')
                            meghdar = meghdar*10 + (file.charAt(i)-48);
                        else
                        {
                            i++;
                            shart=1;
                            while(47<file.charAt(i) && file.charAt(i)<58)
                            {
                                meghdar = meghdar*10 + (file.charAt(i)-48);
                                tavan++;
                                i++;
                            }
                        }
                        if (shart == 0)
                        i++;
                    }
                    meghdar=meghdar/(Math.pow(10,tavan));
                    if (shart2 == 1)
                        meghdar=-1 * meghdar;
                    i--;
                    dadeh.put(key, meghdar);
                } else if (file.charAt(i+2) == '{') {
                    i=i+2;
                    dadeh.put(key,analizor(i));
                    int new_i=1;
                    while (new_i != 0)
                    {
                        i++;
                        if (file.charAt(i) == '{')
                            new_i++;
                        if (file.charAt(i) == '}')
                            new_i--;
                    }
                } else
                {
                    i=i+2;
                    dadeh.put(key,analiz(i));
                    int new_i=1;
                    while (new_i != 0)
                    {
                        i++;
                        if (file.charAt(i) == '[')
                            new_i++;
                        if (file.charAt(i) == ']')
                            new_i--;
                    }
                }
            }
            i++;
        }
        return dadeh;
    }
    public static ArrayList analiz(int i)
    {
        ArrayList<Object> dadeh=new ArrayList<Object>();
        i++;
        while (file.charAt(i) != ']')
        {
            if ((47<file.charAt(i) && file.charAt(i)<58) || file.charAt(i) == '-')
            {
                double meghdar=0;
                int tavan=0;
                int shart=0;
                int shart2=0;
                if (file.charAt(i) == '-')
                {
                    i++;
                    shart2=1;
                }
                while((47<file.charAt(i) && file.charAt(i)<58) || file.charAt(i) == '.')
                {
                    if (file.charAt(i) != '.')
                        meghdar = meghdar*10 + (file.charAt(i)-48);
                    else
                    {
                        i++;
                        shart=1;
                        while(47<file.charAt(i) && file.charAt(i)<58)
                        {
                            meghdar = meghdar*10 + (file.charAt(i)-48);
                            tavan++;
                            i++;
                        }
                    }
                    if (shart ==0)
                    i++;
                }
                meghdar=meghdar/(Math.pow(10,tavan));
                if (shart2 == 1)
                    meghdar = -1 * meghdar;
                i--;
                dadeh.add(meghdar);
            }
            if (file.charAt(i) == 't' || file.charAt(i) == 'f')
            {
                if (file.charAt(i) == 't')
                {
                    boolean bol=true;
                    dadeh.add(bol);
                }
                else
                {
                    boolean bol=false;
                    dadeh.add(bol);
                }
            }
            if (file.charAt(i) == '"')
            {
                i++;
                int j=i;
                while (file.charAt(i) != '"')
                {
                    i++;
                }
                String str=file.substring(j,i);
                dadeh.add(str);
            }
            if (file.charAt(i) == '[')
            {
                dadeh.add(analiz(i));
                int new_i=1;
                while (new_i != 0)
                {
                    i++;
                    if (file.charAt(i) == '[')
                        new_i++;
                    if (file.charAt(i) == ']')
                        new_i--;
                }
            }
            if (file.charAt(i) == '{')
            {
                dadeh.add(analizor(i));
                int new_i=1;
                while (new_i != 0)
                {
                    i++;
                    if (file.charAt(i) == '{')
                        new_i++;
                    if (file.charAt(i) == '}')
                        new_i--;
                }
            }
            i++;
        }
        return dadeh;
    }
    public static StringBuilder tabdil(StringBuilder file1)
    {
        StringBuilder file=new StringBuilder();
        for (int i=0;i<file1.length();i++)
        {
            if (file1.charAt(i) == '"') {
                int j = i;
                i++;
                while (file1.charAt(i) != '"') {
                    i++;
                }
                file.append(file1.substring(j,i));
            }
            if (file1.charAt(i) != ' ')
                file.append(file1.charAt(i));
        }
        return file;
    }
}
